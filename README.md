# BT Call Charges

## 'Data sources':

* [http://www.bt.com/pricing/current/Call_Charges_boo/2-0016_d0e19859.htm](http://www.bt.com/pricing/current/Call_Charges_boo/2-0016_d0e19859.htm)
* [http://www.bt.com/pricing/current/Call_Charges_boo/2-0016_d0e5.htm](http://www.bt.com/pricing/current/Call_Charges_boo/2-0016_d0e5.htm)

## String replacements:

* '-Calls to Mobile telephones (a)'
* '- Calls to Mobile telephones (a)'
* '- Calls to Mobile telephones'
* ' Calls to Mobile telephones'
* ' - Calls to wifi services(a)'
* ' - Calls to wifi services'
* '-calls to mobile telephones'
* '-calls to premium rate services'
* '-calls to internet services'
* '-calls not including internet services'
* '-Calls to Personal Numbering Services'
* ' and mobile telephones'
* ' (a)'
* ' - Calls to New Voice Services'
* ' and information services'
* ' - Calls to Premium Rate Services'
* '-Calls to Pagers and Voice Messaging Services'
* '-Calls to 101 "Non Emergency Services"'

